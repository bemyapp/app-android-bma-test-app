package com.bemyapp.bmatestapp;

import com.bemyapp.sdk.BMA;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class BMAtestAppActivity2 extends Activity {
	
	BMA mapub = null;
    public Context context = this;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
              
        setContentView(R.layout.page2);
        
        //BMA.enableVerboseMode(true);
        //BMA.setBannerTimeout(30);
        //BMA.useAppLocker("Application is locked ! Click on button below to get a working copy.", "market://details?id=com.b1project.ominfo", "Unlock");
        BMA.setBannerMargin(0);
        BMA.reverseBanner();
        mapub = new BMA("demoBMA", getBaseContext());
        
        Button mButton = (Button) findViewById(R.id.button1);
        mButton.setOnClickListener(mButtonClick);
		LinearLayout layout=(LinearLayout)findViewById(R.id.pubLayout);
        mapub.getBanner(layout);
    }
    View.OnClickListener mButtonClick = new View.OnClickListener() {
        public void onClick(View v) {
        	setResult(RESULT_OK);
        	finish();
        }
    };
    
    @Override
    protected void onResume() {
    	super.onResume();
    }
}