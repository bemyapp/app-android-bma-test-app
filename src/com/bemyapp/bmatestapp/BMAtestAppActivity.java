package com.bemyapp.bmatestapp;

import com.bemyapp.sdk.BMA;
import com.bemyapp.sdk.BMABannerView;
import com.bemyapp.sdk.BMAListener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class BMAtestAppActivity extends Activity implements BMAListener, OnSeekBarChangeListener {
	
	BMA mapub = null;
    public Context context = this;
    private LinearLayout ll;
    private static boolean banner_reversed = false;
    private TextView logView;
    private final static int BANNER_FULL = 0;
    private final static int BANNER_TXT = 1;
    private final static int BANNER_IMG = 2;
    private static int BANNER_TYPE = BANNER_FULL;
    private SeekBar sb;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewServer.get(this).addWindow(this);
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/      
        setContentView(R.layout.main);
        logView = (TextView) findViewById(R.id.textView4);
        Button mButton = (Button) findViewById(R.id.button1);
        mButton.setOnClickListener(mButtonClick);
    	
        /**
         * Initializing BeMyApp SDK
         */
        mapub = new BMA("demoBMA", getBaseContext());
        mapub.setBMAListener(this);
        
        /**
         * Global BMA preferences 
         */
        BMA.enableVerboseMode(true);
        //BMA.useAppLocker("Application is locked ! Click on button below to get a working copy.", "market://details?id=com.b1project.ominfo", "Unlock");
        /**/
        
        
        BMA.setBannerTimeout(120);
        //BMA.setBannerMargin(10);
        
        BMA.setFsRate(60);
        mapub.getFullscreen();
        
		Log.i("BMATest", mapub.toString());
   	 	ll = (LinearLayout) findViewById(R.id.pubLayout);
   	 	
   	 	sb = (SeekBar) findViewById(R.id.seekBar1);
   	 	sb.setOnSeekBarChangeListener(this);
    }
    
    View.OnClickListener mButtonClick = new View.OnClickListener() {
        public void onClick(View v) {
        	Intent i = new Intent(context, BMAtestAppTabActivity.class);
        	try{
        		startActivity(i);
        	}
        	catch(Exception e){
                   		
        	}
        }
    };
    
    @Override
    protected void onResume(){
    	super.onResume();
    	ViewServer.get(this).setFocusedWindow(this);
        BMABannerView banner = mapub.getBannerView();
		//banner_reversed = false;
		BMA.reverseBanner(banner_reversed);
		FrameLayout.LayoutParams llp = (FrameLayout.LayoutParams) ll.getLayoutParams();
	    llp.gravity = (banner_reversed)?Gravity.BOTTOM:Gravity.TOP;
	    ll.setLayoutParams(llp);
	    switch(BANNER_TYPE){
		    case BANNER_FULL:
		    	setFULLBanner(banner);
		    	break;
		    case BANNER_TXT:
		    	setTXTBanner(banner);
		    	break;
		    case BANNER_IMG:
		    	setIMGBanner(banner);
		    	break;
	    }
        
    }
    
    @Override
    protected void onPause(){
    	super.onPause();
    	mapub.onPause();
    }
	
	public void setTXTBanner(View v){
		BMA.setBannerAnimated(false);
        BMA.setBannerNoText(false);
        BANNER_TYPE = BANNER_TXT;
	    mapub.getBanner(ll);
	}
	
	public void setIMGBanner(View v){
        BMA.setBannerNoText(true);
        BANNER_TYPE = BANNER_IMG;
	    mapub.getBanner(ll);
        BMABannerView banner = mapub.getBannerView();
        if(banner != null){
        	banner.unshrink();
        }
	}
	
	public void setFULLBanner(View v){
		BMA.setBannerAnimated(true);
		BANNER_TYPE = BANNER_FULL;
	    mapub.getBanner(ll);
	}
	
	public void showFS(View v){
        mapub.getFullscreen();		
	}
	
	public void swapBannerPosition(View v){
		banner_reversed = (banner_reversed)?false:true;
		BMA.reverseBanner(banner_reversed);
		FrameLayout.LayoutParams llp = (FrameLayout.LayoutParams) ll.getLayoutParams();
	    llp.gravity = (banner_reversed)?Gravity.BOTTOM:Gravity.TOP;
	    ll.setLayoutParams(llp);
	    mapub.getBanner(ll);
        BMABannerView banner = mapub.getBannerView();
        if(banner != null){
        	banner.unshrink();
        }
	}
	
	public void resetMargin(View v){
		sb.setProgress(50);
	}

	@Override
	public void onApplicationLinked(boolean arg0) {
		// TODO Auto-generated method stub
		if(arg0 == true){
			Log.i("BMATest", "app linked");
			logView.setText(logView.getText()+"- Application linked\n");
		}
		else{
			Log.i("BMATest", "app not linked");
			logView.setText(logView.getText()+"- Application not linked\n");
		}
    }

	@Override
	public void onBannerCreated() {
		logView.setText(logView.getText()+"- Banner created\n");
	}
	
	@Override
	public void onIntersticialCreated() {
		logView.setText(logView.getText()+"- Intersticial created\n");
	}

	@Override
	public void onIntersticialQuit() {
		logView.setText(logView.getText()+"- Intersticial quit\n");	
	}

	@Override
	public void onBannerExpanded() {
		logView.setText(logView.getText()+"- Banner expanded\n");
	}

	@Override
	public void onBannerCollapsed() {
		logView.setText(logView.getText()+"- Banner collapsed\n");
	}

	@Override
	public void onApplicationLocked() {
		logView.setText(logView.getText()+"- Application locked\n");
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		BMA.setBannerMargin(progress - 50);
        BMABannerView banner = mapub.getBannerView();
        if(banner != null){
        	//banner.unshrink();
        	banner.refresh();
        }
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}
}