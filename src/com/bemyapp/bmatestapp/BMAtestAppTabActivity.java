package com.bemyapp.bmatestapp;

import com.bemyapp.sdk.BMA;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

@SuppressWarnings("deprecation")
public class BMAtestAppTabActivity extends TabActivity {
	public static final String MAPUB_KEY="demoBMA";
	
	private TabHost tabHost;
	

	protected BMA mapub;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		Resources res = getResources(); 
		

		
		tabHost = getTabHost();
		Intent myIntent = new Intent(this,BMAtestAppActivity2.class);
		tabHost.addTab(tabHost.newTabSpec("by header").setIndicator("By header", 
									res.getDrawable(android.R.drawable.ic_menu_preferences))
								.setContent(myIntent));
		tabHost.addTab(tabHost.newTabSpec("by footer").setIndicator("By footer", 
				res.getDrawable(android.R.drawable.ic_menu_preferences))
			.setContent(myIntent));
		tabHost.setCurrentTab(0);
		
		
        BMA.reverseBanner(true);
        BMA.setBannerNoText(true);

        mapub = new BMA(MAPUB_KEY, getBaseContext());
        mapub.getFullscreen();
	}
}
